# FROM kolemp/docker-php-nginx:7.3
FROM kolemp/php:7.3

# symfony demo app requires sqlite
ENV DEBIAN_FRONTEND=noninteractive 

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
    apt-get update && \
    apt-get install -y --force-yes  -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" php${PHP_VERSION}-sqlite3 && \
        rm -rf /var/lib/apt/lists/*

ENV APP_ENV=prod

ARG COMPOSE_HASH

# this way vendors will be updated only if composer.lock changes
RUN echo "compose hash: $COMPOSE_HASH"
COPY ./composer* /data/application/

RUN mkdir -p var vendor /var/www/.composer  && chown www-data:www-data var vendor /var/www/.composer

USER www-data

RUN composer install --no-dev --no-interaction --no-progress --no-suggest --optimize-autoloader --no-scripts

COPY ./ /data/application/
# This time vendors are already there so this will only trigger script
RUN composer install --no-dev --no-interaction --no-progress --no-suggest --optimize-autoloader

USER root
RUN ln -s /data/application/public /data/application/web && ls -la
