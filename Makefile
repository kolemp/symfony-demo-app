now := ${shell date +%F_%H%M%S}
IMAGE_TAG ?= latest
IMAGE_NAME ?= kolemp/symfony-demo-app

build:
	docker build --pull -t ${IMAGE_NAME}:${IMAGE_TAG} --build-arg COMPOSE_HASH=$$(md5sum composer.lock| cut -d ' ' -f 1) .

push:
	docker push ${IMAGE_NAME}:${IMAGE_TAG}
